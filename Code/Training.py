import tflearn as tf
import glob
import os

#Training data and return model to predict
#Model are saved in saved_model folder, if there no saved model the class will perform a training.
#Author: Luu Thanh Son - 14520772

class Training:
    def __init__(self, X, Y):
        self.X = X
        self.Y = Y

    def training(self):
        #Building neural network
        network = tf.input_data(shape=[None, 28, 28, 1], name='input')
        network = tf.conv_2d(network, 32, 3, activation='relu', regularizer="L2")
        network = tf.max_pool_2d(network, 2)
        network = tf.local_response_normalization(network)
        network = tf.conv_2d(network, 64, 3, activation='relu', regularizer="L2")
        network = tf.max_pool_2d(network, 2)
        network = tf.local_response_normalization(network)
        network = tf.fully_connected(network, 128, activation='tanh')
        network = tf.dropout(network, 0.8)
        network = tf.fully_connected(network, 256, activation='tanh')
        network = tf.dropout(network, 0.8)
        network = tf.fully_connected(network, 10, activation='softmax')
        network = tf.regression(network, optimizer='adam', learning_rate=0.01,loss='categorical_crossentropy', name='target')

        model = tf.DNN(network, tensorboard_verbose=0)

        #if there saved model then load in, not training again
        if glob.glob(os.getcwd() + '/Model/ocr_mnist*'):
            model.load(os.getcwd() + '/Model/ocr_mnist')
            return model

        model.fit(self.X, self.Y, n_epoch=20, show_metric=True)
        model.save(os.getcwd() + '/Model/ocr_mnist')
        return model